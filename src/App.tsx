import React from 'react';
import appStyles from './styles/pages/App.module.scss';
import styles from './styles/globals/Container.module.scss';
import Button from './components/Button';
import Heading from './components/Heading';

const levels: (1 | 2 | 3)[] = [1, 2, 3];

function App() {
  return (
    <div>
      <header className={appStyles.Header}>
        <h1>Styling Design Systems</h1>
      </header>

      <main className={styles.container}>
        <section>
          <h1>Buttons</h1>

          <div className={appStyles['buttons-preview']}>
            <Button variant="solid">Solid</Button>
            <Button variant="outline">Outline</Button>
          </div>
        </section>

        <hr />

        <section>
          <h1>Headings</h1>

          {levels.map((level) => (
            <div>
              <Heading level={level}>Heading {level}</Heading>
            </div>
          ))}
        </section>
      </main>
    </div>
  );
}

export default App;
