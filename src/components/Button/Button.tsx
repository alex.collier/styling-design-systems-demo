import React, { FC } from 'react';
import styles from './Button.module.scss';
import clsx from 'clsx';

interface ButtonProps {
  variant: 'solid' | 'outline';
}

const Button: FC<ButtonProps> = (props) => {
  return (
    <button className={clsx(styles.button, styles[`button--${props.variant}`])}>
      {props.children}
    </button>
  );
};

export default Button;
