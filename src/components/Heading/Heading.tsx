import React, { FC } from 'react';
import styles from './Heading.module.scss';

interface HeadingProps {
  level: 1 | 2 | 3;
  tag?: 'h1' | 'h2' | 'h3' | 'div' | 'p';
}

const Heading: FC<HeadingProps> = (props) => {
  const Tag = props.tag || `h${props.level}`;

  return (
    <Tag className={styles[`heading-${props.level}`]}>{props.children}</Tag>
  );
};

export default Heading;
